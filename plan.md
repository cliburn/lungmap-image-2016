# Segmentation and feature recognition of immunofluoresecent LungMAP images for  annotation and pattern discovery

## What has been done in Data+ 2016

For the data_plus 2016 project, the students met two objectives

1. Create a [database](data_plus/metadata_table_all.csv) of image metadata that can be used to find out information about an image or filter for images that meet specific criteria.
2. Extract meaningful image segments using a combination of prior knowledge constraints (e.g. magnification, probes used) and basic pipelines (preprocessing, thresholding, contour finding) based on OpenCV that can extract the features:
    1. [Bronchioles](data_plus/BronchiolesSegmented.pdf)
    2. [Acinar Tubules](data_plus/segmentation_AcinarTubule.pdf)
    3. [Veins](data_plus/VeinSegmented.pdf)
    4. [Club cells](data_plus/segmentation_ClubCells.pdf)
    5. [Pneumocyte](data_plus/segmentation_pneumocyte.pdf)

## Propose project extension

We want to build on this foundation to achieve the following objectives:

- Increase the accuracy of image segmentation
- Increase the number of features recognized
- Gain ability to recognize multiple features within an image
- Summarize how image features change over the course of lung development
- Make the feature recognition pipeline accessible to users

Specifically, the objective is to train a **classifier** using a large curated set of positive and negative image samples. To get the samples, we propose to use an image segmentation pipeline to extract features and manually classify these features as positive (true positives) or negative (false positive) samples. After training the classifier to an acceptable accuracy, a cascade classification algorithm will be applied to identify the features in new images. Many such classifiers need to be trained, one for each distinct image feature. Post-processing using information from multiple classifiers may be used to further improve accuracy - for example, via use of spatial hierarchical information (e.g. a bronchiolar epithelial cell must be within a bronchus structure, or that two cell types are often found in close proximity to each other). In addition, metadata constraints for each image will be used to select the appropriate classifiers for each image.

Our strategy will be to focus on just **one feature at a time** - we believe that it is of more productive to construct robust/accurate pipelines to recognize a single image feature before moving on to another. In other words, we will iterate through the first two stages for each feature of interest.

### Stage 1: Improve image segmentation pipeline

The initial focus will be on exploring preprocessing and image segmentation methods beyond the thresholding and contour finding to see how much we can improve accuracy by making better use of prior knowledge and more sophisticated segmentation methods. Specific algorithms that will be tested are:

- adaptive threshold and contour
- watershed with knowledge constrained markers
- random walker (anisotropic diffusion)
- two-stage algorithms (superpixels)
    - Felzenszwalb’s efficient graph based segmentation
    - Quickshift image segmentation
    - SLIC - K-Means based image segmentation

These segmentation algorithms may be combined with preprocessing to improve accuracy and/or robustness, and we will evaluate the impact of

- gamam and log contrast adjustment
- denoising
- histogram equalization and matching
- morphological operations (dilation, erosion, opening, closing)
- color deconvolution for immunohistochemical staining colors separation

### Stage 2: Develop fast feature recognition for new images

If sufficiently accurate, feature recognition can be achieved by just applying the segmentation pipeline for each feature to a new image. However, the image segmentation pipeline is likely to be too slow for real-time feature recognition, and so we will develop fast feature recognition pipelines as well.

We begin by extracting image samples containing features using a bounding box approach, and manually classify each image sample as either a positive or negative sample for that feature (may require LungMAP member expertise). We will then evaluate the following strategies using these samples to train classifiers for object recognition:

- Template matching
- Oriented FAST and Rotated BRIEF (ORB) feature matching with homography
    - brute force matching
    - Fast Approximate Nearest Neighbor (FLANN) matching
- Cascade classifier using boosted trees
- TensorFlow deep learning for feature recognition

Prior knowledge, spatial and hierarchical relationships will be used in either pre- or post-processing steps to improve feature recognition accuracy.

### Data analysis and tool development

Data for processed images will be stored in a database and accessible via a REST API. We will work with RTI team to develop a standard data representation for image features that RTI can use to develop annotation overlays for online image visualization.

How image features change over time will be analyzed using time series plots or heatmaps. Features of interest include count, area, and (possibly) more complex characterizations such as morphology and spatial distribution statistics.

### Timeline

- Segmentation and classifier pipeline for first feature (3 months)
- Optimization of pipeline and inclusion of additional features (3 months)
- Data analysis, tool development and write-up (3 months)

### Responsibilities

#### Cliburn Chan (No additional effort requested)

- Planning, oversight, mentoring
- Weekly meeting with Lina, Ben and Scott

#### Lina Yang (up to 40 hours per week for remainder of summer; 10 hours per week during term)

- Develop and evaluate segmentation pipeline
- Data analysis for patterns of change over time

#### Ben Neely and Scott White (10% effort each for 9 months)

- Develop and evaluate fast feature recognition methods
- Develop data representation for image annotation
- Database and REST API creation and management

## Appendix: Potential image feature target list (will need LungMAP input as to priorities and rules for identificaiotn)

- Structures
    - Bronchiole
    - Artery
    - Vein
    - Acinar tubule
    - Alveolar duct
    - Alveolus

- Cells
    - Epithelial cells
        - Type 1 penumocyte (Aqyaprin 5/Sftpc/Abca3)
        - Type 2 Pneumocyte (Aquaporin 1/Pdpn/SPA/SPB/SPC/LysoTracker/DND-26)
        - Bronchiolar epithelial
        - Secretory (Scgb1a1/Scgb3a2)
        - Goblet (Mucin 5ac,Mucin 5b)
        - Neuroendocrine (Uchl1/Ascl1/CGRP)
        - Ciliated (Foxp1/Tubb6/Tubulin4)
        - Lung endoderm progenitor (Nkx2.1/Gata6/Foxa1/2)
        - Proximal progenitor )Sox2)
        - Distal progenitor (Sox9/Id2)
        - Basal cell (Cytokeratin 4, cytokeratin 14)
        - Club cell (CCSP, CyP450, 2F2)
    - Vasculature cells
        - smooth muscle
        - pericyte (Pdgf$\beta$/Ng2)
        - proximal endothelium (Vwf)
        - distal endothelium (CD31/VE-cadherin)
        - endothelial progenitor (VE-cadherin)
    - Others
        - Immune cells
